﻿using System;
using System.IO;

namespace ConsoleAppTest
{
    class Program
    {
        static int[] start_pos;
        static int[] end_pos;
        static char[,,] lab;
        static int min_time = int.MaxValue;
        static short h, m, n;
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader("INPUT.txt");
            //считываем габариты лабиринта
            string[] hmn = sr.ReadLine().Split(' ');
            h = short.Parse(hmn[0]);
            m = short.Parse(hmn[1]);
            n = short.Parse(hmn[2]);

            start_pos = new int[3];
            end_pos = new int[3];
            lab = new char[h, m, n];
            string labline;
            //считываем сам лабиринт
            for (int i = 0; i < h; i++)
            {
                for (int i2 = 0; i2 < m; i2++)
                {
                    labline = sr.ReadLine();
                    for (int i3 = 0; i3 < m; i3++)
                    {
                        lab[i, i2, i3] = labline[i3];
                        if (lab[i, i2, i3] == '1')
                        {
                            start_pos[0] = i;
                            start_pos[1] = i2;
                            start_pos[2] = i3;
                        }
                        else if (lab[i, i2, i3] == '2')
                        {
                            end_pos[0] = i;
                            end_pos[1] = i2;
                            end_pos[2] = i3;
                        }
                    }
                }
                labline = sr.ReadLine();
            }
            //вызываем функцию для поиска минимального пути
            end_find(start_pos[0], start_pos[1], start_pos[2], 0);
            StreamWriter sw = new StreamWriter("OUTPUT.txt");
            sw.WriteLine(min_time);
            sw.Close();
            sr.Close();
            Console.ReadKey();
        }

        static void end_find(int z, int y, int x, int time)
        {

            if (lab[z, y, x] == '2' && min_time > time)
            {
                min_time = time;
            }
            else
            {
                //рекурсивно обходим лабиринт
                lab[z, y, x] = 'o';
                if (z < h - 1 && lab[z + 1, y, x] != 'o')
                {
                    end_find(z + 1, y, x, time + 5);
                }
                if (y < m - 1 && lab[z, y + 1, x] != 'o')
                {
                    end_find(z, y + 1, x, time + 5);
                }
                if (x < n - 1 && lab[z, y, x + 1] != 'o')
                {
                    end_find(z, y, x + 1, time + 5);
                }
                //учитываем то, что принц не может двигатся вверх
                if (y > 0 && lab[z, y - 1, x] != 'o')
                {
                    end_find(z, y - 1, x, time + 5);
                }
                if (x > 0 && lab[z, y, x - 1] != 'o')
                {
                    end_find(z, y, x - 1, time + 5);
                }
            }
        }
    }
}
