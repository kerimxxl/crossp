using System;
using System.Collections.Generic;
using System.IO;


namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> arr = new List<int>();
            int N;
            int M;
            var tempS = System.IO.File.ReadAllText("input.txt").Split(' ', '\r', '\n', '\t');
            foreach (string element in tempS)
            {
                if (element == tempS[0])
                {
                    N = Convert.ToInt32(element);
                }
                else if (!string.IsNullOrEmpty(element))
                {
                    Console.WriteLine(string.IsNullOrEmpty(element));
                    //string temp = element.Replace('.', ',');
                    arr.Add(Convert.ToInt32(element));
                }
            }

            N = arr[0];
            M = arr[1];

            if ((N + M) == ((N + M) / 2) * 2)
            {
                System.IO.File.WriteAllText("output.txt", "2");
            }
            else
            {
                System.IO.File.WriteAllText("output.txt", "1");
            }

        }
    }
}
