﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Analytics.Interfaces;
using Microsoft.Analytics.Interfaces.Streaming;
using Microsoft.Analytics.Types.Sql;

namespace myproject
{
   Usage: dotnet[options]
Usage: dotnet[path - to - application]

Options:
-h|--help Display help.
--info Display.NET information.
--list-sdks Display the installed SDKs.
--list-runtimes Display the installed runtimes.

path-to-application:
The path to an application.dll file to execute.
        }
    }
}